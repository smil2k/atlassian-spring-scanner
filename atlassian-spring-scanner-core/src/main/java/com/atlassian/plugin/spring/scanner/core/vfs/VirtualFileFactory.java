package com.atlassian.plugin.spring.scanner.core.vfs;

import java.io.File;
import javax.annotation.processing.Filer;

/**
 * A virtual file interface to abstract the differences between Javac Filer disk access and bog standard File access.
 * <p/>
 * This is the factory to get virtual files from.
 */
public class VirtualFileFactory
{
    private final Filer filer;
    private final File baseDir;

    public VirtualFileFactory(File baseDir)
    {
        this.filer = null;
        this.baseDir = baseDir;
    }

    public VirtualFileFactory(final Filer filer)
    {
        this.filer = filer;
        this.baseDir = null;
    }

    public VirtualFile getFile(String fileName)
    {
        if (filer != null)
        {
            return new FilerBasedVirtualFile(filer, fileName);
        }
        else
        {
            File file = new File(baseDir,fileName);
            return new FileBasedVirtualFile(file);
        }
    }

}
