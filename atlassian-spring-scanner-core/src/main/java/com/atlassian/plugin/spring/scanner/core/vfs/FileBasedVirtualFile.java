package com.atlassian.plugin.spring.scanner.core.vfs;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Properties;

/**
 */
class FileBasedVirtualFile implements VirtualFile
{
    private final File file;

    FileBasedVirtualFile(final File file)
    {
        this.file = file;
    }

    @Override
    public Collection<String> readLines() throws IOException
    {
        FileReader reader;
        try
        {
            reader = new FileReader(file);
        }
        catch (FileNotFoundException e)
        {
            return Collections.emptyList();
        }
        return CommonIO.readLines(reader);
    }

    @Override
    public void writeLines(final Iterable<String> lines) throws IOException
    {
        mkdirs(file);
        CommonIO.writeLines(new FileWriter(file), lines);
    }

    @Override
    public void writeProperties(final Properties properties, final String comment) throws IOException
    {
        mkdirs(file);
        CommonIO.writeProperties(new FileWriter(file), properties, comment);
    }

    private void mkdirs(final File file) throws IOException
    {
        File parentDir = file.getParentFile();
        //noinspection ResultOfMethodCallIgnored
        parentDir.mkdirs();
        if (!parentDir.exists())
        {
            throw new IOException("Unable to create output directory " + parentDir);
        }
    }
}
