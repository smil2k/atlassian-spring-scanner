import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import com.atlassian.plugin.spring.scanner.test.DefaultAction;

import org.junit.Test;

import static org.apache.commons.io.IOUtils.readLines;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;

public class TestExpectedIndexFiles
{
    @Test
    public void testExpected() throws Exception
    {
        assertIndexContents("component",
            "com.atlassian.plugin.spring.scanner.test.ConsumingInternalOnlyComponent",
            "com.atlassian.plugin.spring.scanner.test.ConsumingMixedComponents",
            "com.atlassian.plugin.spring.scanner.test.ExposedAsAServiceComponent",
            "com.atlassian.plugin.spring.scanner.test.ExposedAsAServiceComponentWithSpecifiedInterface",
            "com.atlassian.plugin.spring.scanner.test.ExposedAsAServiceComponentWithMultipleSpecifiedInterfaces",
            "com.atlassian.plugin.spring.scanner.test.ExposedAsADevServiceComponent",
            "com.atlassian.plugin.spring.scanner.test.external.ExportExternalComponentsViaFields$ExternalServiceViaField",
            "com.atlassian.plugin.spring.scanner.test.external.ExportExternalComponentsViaFields$ExternalDevServiceViaField",
            "com.atlassian.plugin.spring.scanner.test.InternalComponent",
            "com.atlassian.plugin.spring.scanner.test.InternalComponent2",
            "com.atlassian.plugin.spring.scanner.test.NamedComponent#namedComponent",
            "com.atlassian.plugin.spring.scanner.test.NamedConsumingMixedComponents",
            "com.atlassian.plugin.spring.scanner.test.OuterClass$InnerClass$InnerComponent$EvenMoreInnerComponent",
            "com.atlassian.plugin.spring.scanner.test.OuterClass$InnerClass$InnerComponent",
            "com.atlassian.plugin.spring.scanner.test.OuterClass$OuterComponent",

            "com.atlassian.plugin.spring.scanner.test.fields.ComponentWithFields",

            "com.atlassian.plugin.spring.scanner.test.lots.LotsOfComponents1",
            "com.atlassian.plugin.spring.scanner.test.lots.LotsOfComponents2",
            "com.atlassian.plugin.spring.scanner.test.lots.LotsOfComponents3",
            "com.atlassian.plugin.spring.scanner.test.lots.LotsOfComponents4",
            "com.atlassian.plugin.spring.scanner.test.lots.LotsOfComponents5",
            "com.atlassian.plugin.spring.scanner.test.lots.LotsOfComponents6",
            "com.atlassian.plugin.spring.scanner.test.lots.LotsOfComponents7",
            "com.atlassian.plugin.spring.scanner.test.lots.LotsOfComponents8",
            "com.atlassian.plugin.spring.scanner.test.lots.LotsOfComponents9",

            "com.atlassian.plugin.spring.scanner.test.spring.ControllerComponent",
            "com.atlassian.plugin.spring.scanner.test.spring.RepositoryComponent",
            "com.atlassian.plugin.spring.scanner.test.spring.ServiceComponent",

            "com.atlassian.plugin.spring.scanner.test.jsr.JsrComponent",

            "com.atlassian.plugin.spring.scanner.external.component.ExternalJarComponent1",
            "com.atlassian.plugin.spring.scanner.external.component.ExternalJarComponent2",
            "com.atlassian.plugin.spring.scanner.external.component.ExternalJarComponentComposite",
            "com.atlassian.plugin.spring.scanner.test.external.ConsumesExternalComponentsViaConstructor",
            "com.atlassian.plugin.spring.scanner.test.external.ConsumesExternalComponentsViaFields");
    }

    @Test
    public void testExpectedImports() throws Exception
    {
        assertIndexContents("imports",
            "com.atlassian.jira.bc.issue.IssueService",
            "com.atlassian.jira.bc.issue.comment.CommentService",
            "com.atlassian.jira.bc.issue.link.IssueLinkService",
            "com.atlassian.jira.util.I18nHelper$BeanFactory");
    }

    @Test
    public void testExpectedExports() throws Exception
    {
        assertIndexContents("exports",
                "com.atlassian.plugin.spring.scanner.test.ExposedAsAServiceComponent",
                "com.atlassian.plugin.spring.scanner.test.ExposedAsAServiceComponentWithSpecifiedInterface#"
                        + "com.atlassian.plugin.spring.scanner.test.ExposedAsAServiceComponentInterface",
                "com.atlassian.plugin.spring.scanner.test.ExposedAsAServiceComponentWithMultipleSpecifiedInterfaces#"
                        + "com.atlassian.plugin.spring.scanner.test.ExposedAsAServiceComponentInterface,"
                        + "com.atlassian.plugin.spring.scanner.test.ExposedAsASecondComponentInterface",
                "com.atlassian.plugin.spring.scanner.test.external.ExportExternalComponentsViaFields$ExternalServiceViaField");
    }

    @Test
    public void testExpectedDevExports() throws Exception
    {
        assertIndexContents("dev-exports",
                "com.atlassian.plugin.spring.scanner.test.ExposedAsADevServiceComponent",
                "com.atlassian.plugin.spring.scanner.test.external.ExportExternalComponentsViaFields$ExternalDevServiceViaField");
    }

    @Test
    public void testExpectedBootStrapComponents() throws Exception
    {
        assertIndexContents("profile-bootstrap/component",
            "com.atlassian.plugin.spring.scanner.test.bootstrap.BootstrappingComponent",
            "com.atlassian.plugin.spring.scanner.test.moduletype.BasicModuleTypeFactory",
            // because we have a @ModuleType, we get an extra component in support of that
            "com.atlassian.plugin.osgi.bridge.external.SpringHostContainer");
    }

    @Test
    public void testExpectedBootStrapImports() throws Exception
    {
        assertIndexContents("profile-bootstrap/imports",
            "com.atlassian.event.api.EventPublisher",
            "com.atlassian.plugin.PluginAccessor",
            "com.atlassian.jira.util.I18nHelper$BeanFactory");
    }

    @Test
    public void testExpectedPackageInfoComponents() throws Exception
    {
        //
        // these two use package info level annotations
        assertIndexContents("profile-unused/component",
            "com.atlassian.plugin.spring.scanner.test.unused.UnusedComponent1",
            "com.atlassian.plugin.spring.scanner.test.unused.UnusedComponent2");

        //
        // this specific class has another profile override and hence does NOT take the package level ones
        //
        // It cumulative inheritance, its override specifics
        //
        assertIndexContents("profile-specific-override/component",
            "com.atlassian.plugin.spring.scanner.test.unused.UnusedSpecificOverride");
    }

    @Test
    public void testBambooComponents() throws Exception
    {
        assertIndexContents("component-bamboo", "com.atlassian.plugin.spring.scanner.test.BambooOnlyComponent");
    }

    @Test
    public void testConfluenceComponents() throws Exception
    {
        assertIndexContents("component-confluence", "com.atlassian.plugin.spring.scanner.test.ConfluenceOnlyComponent");
    }

    @Test
    public void testFecruComponents() throws Exception
    {
        assertIndexContents("component-fecru", "com.atlassian.plugin.spring.scanner.test.FecruOnlyComponent");
    }

    @Test
    public void testJiraComponents() throws Exception
    {
        assertIndexContents("component-jira",  "com.atlassian.plugin.spring.scanner.test.JiraOnlyComponent");
    }

    @Test
    public void testRefappComponents() throws Exception
    {
        assertIndexContents("component-refapp", "com.atlassian.plugin.spring.scanner.test.RefappOnlyComponent");
    }

    @Test
    public void testStashComponents() throws Exception
    {
        assertIndexContents("component-stash", "com.atlassian.plugin.spring.scanner.test.StashOnlyComponent");
    }

    private void assertIndexContents(final String indexFilename, final String... expected)
            throws IOException
    {
        final List<String> actual = readIndexFile(indexFilename);
        assertThat(actual, containsInAnyOrder(expected));
    }

    private List<String> readIndexFile(final String indexFileName) throws IOException
    {
        final String resourcePath = "META-INF/plugin-components/" + indexFileName;
        final InputStream inputStream = DefaultAction.class.getClassLoader().getResourceAsStream(resourcePath);
        assertThat("Unable to read expected index file '" + resourcePath + "'", inputStream, notNullValue());
        // IOUtils is not generic in the version we are using
        //noinspection unchecked
        return readLines(inputStream);
    }
}
