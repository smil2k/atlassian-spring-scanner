package com.atlassian.plugin.spring.scanner.test;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsDevService;

import org.springframework.stereotype.Component;

@ExportAsDevService
@Component
public class ExposedAsADevServiceComponent
{
}
