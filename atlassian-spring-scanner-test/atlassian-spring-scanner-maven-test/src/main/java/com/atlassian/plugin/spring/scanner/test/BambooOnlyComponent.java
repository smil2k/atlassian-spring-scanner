package com.atlassian.plugin.spring.scanner.test;

import com.atlassian.plugin.spring.scanner.annotation.component.BambooComponent;

/**
 * A component that should only be in Bamboo
 */
@BambooComponent
public class BambooOnlyComponent
{
}
