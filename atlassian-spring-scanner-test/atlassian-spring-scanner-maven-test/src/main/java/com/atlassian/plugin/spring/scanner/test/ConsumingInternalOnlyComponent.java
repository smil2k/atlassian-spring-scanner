package com.atlassian.plugin.spring.scanner.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ConsumingInternalOnlyComponent
{

    private final InternalComponent internalComponent;
    private final InternalComponent2 internalComponent2;

    @Autowired
    public ConsumingInternalOnlyComponent(final InternalComponent internalComponent, final InternalComponent2 internalComponent2)
    {
        this.internalComponent = internalComponent;
        this.internalComponent2 = internalComponent2;
    }
}
