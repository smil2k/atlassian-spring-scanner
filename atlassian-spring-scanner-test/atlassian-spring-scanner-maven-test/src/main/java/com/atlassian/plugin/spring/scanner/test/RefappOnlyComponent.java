package com.atlassian.plugin.spring.scanner.test;

import com.atlassian.plugin.spring.scanner.annotation.component.RefappComponent;

/**
 * A component that should only be in Refapp
 */
@RefappComponent
public class RefappOnlyComponent
{
}
