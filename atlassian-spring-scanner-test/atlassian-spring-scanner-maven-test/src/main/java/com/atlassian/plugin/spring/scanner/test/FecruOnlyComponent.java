package com.atlassian.plugin.spring.scanner.test;

import com.atlassian.plugin.spring.scanner.annotation.component.FecruComponent;

/**
 * A component that should only be in Fecru
 */
@FecruComponent
public class FecruOnlyComponent
{
}
