package com.atlassian.plugin.spring.scanner.test;

import org.springframework.stereotype.Component;

/**
 * This tests that fact that we can handle OuterClass$InnerClass$InnerComponent properly
 */
@SuppressWarnings ("UnusedDeclaration")
public class OuterClass
{
    @Component
    public static class OuterComponent
    {

    }

    public static class InnerClass {

        @Component
        public static class InnerComponent
        {

            @Component
            public static class EvenMoreInnerComponent
            {

            }

        }
    }
}
