package com.atlassian.plugin.spring.scanner.processor;

import com.atlassian.plugin.spring.scanner.util.CommonConstants;

import java.lang.annotation.Annotation;
import java.util.Set;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;

/**
 * Handles *ComponentImport annotations and creates class index files for them. Cross product imports are listed in
 * META-INF/plugin-components/componentimport Product specific imports are listed in
 * META-INF/plugin-components/componentimport-${productName}
 * <p/>
 * Entries in these files are either just the fully qualified class name of the component, or the fully qualified
 * class name plus the bean name defined as the value of the annotation separated by #
 * <p/>
 * Example:
 * <p/>
 * com.some.component.without.a.name.MyClass com.some.component.with.a.name.MyClass#myBeanName
 */
@SuppressWarnings ("UnusedDeclaration")
@SupportedAnnotationTypes ("com.atlassian.plugin.spring.scanner.annotation.imports.*")
public class ComponentImportAnnotationProcessor extends IndexWritingAnnotationProcessor
{
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv)
    {

        doProcess(annotations, roundEnv, CommonConstants.COMPONENT_IMPORT_KEY);

        return false;
    }

    @Override
    public TypesAndAnnotation getTypeAndAnnotation(Element element, TypeElement anno)
    {
        TypeElement typeElement;
        Annotation annotation;
        TypesAndAnnotation typesAndAnnotation = null;
        Class componentAnnotationClass;
        try
        {
            componentAnnotationClass = Class.forName(anno.getQualifiedName().toString());
        }
        catch (ClassNotFoundException e)
        {
            return typesAndAnnotation;
        }

        if (element instanceof VariableElement)
        {
            VariableElement variableElement = (VariableElement) element;


            typeElement = (TypeElement) processingEnv.getTypeUtils().asElement(variableElement.asType());

            if (isConstructorParameter(variableElement) || isClassField(variableElement))
            {
                annotation = variableElement.getAnnotation(componentAnnotationClass);
                typesAndAnnotation = new TypesAndAnnotation(typeElement, getContainingClass(variableElement), annotation);
            }
        }

        return typesAndAnnotation;
    }

    private boolean isConstructorParameter(final VariableElement variableElement)
    {
        return ElementKind.PARAMETER.equals(variableElement.getKind()) && ElementKind.CONSTRUCTOR.equals(variableElement.getEnclosingElement().getKind());
    }

    private boolean isClassField(final VariableElement variableElement)
    {
        return ElementKind.FIELD.equals(variableElement.getKind());
    }

}
