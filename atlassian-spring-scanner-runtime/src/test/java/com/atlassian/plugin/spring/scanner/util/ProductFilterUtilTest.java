package com.atlassian.plugin.spring.scanner.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Assume;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.fail;

/**
 * Just a smoke test to catch if the classnames we use to determine which product we're in ever change
 */
@RunWith(MockitoJUnitRunner.class)
public class ProductFilterUtilTest
{
    @Test
    public void testBambooClassIsPresent()
    {
        assertClassIsPresentOnClasspath(ProductFilterUtil.CLASS_ON_BAMBOO_CLASSPATH, "Bamboo");
    }

    @Test
    public void testConfluenceClassIsPresent()
    {
        assertClassIsPresentOnClasspath(ProductFilterUtil.CLASS_ON_CONFLUENCE_CLASSPATH, "Confluence");
    }

    @Test
    public void testFecruClassIsPresent()
    {
        assertClassIsPresentOnClasspath(ProductFilterUtil.CLASS_ON_FECRU_CLASSPATH, "Fecru");
    }

    @Test
    public void testJiraClassIsPresent()
    {
        assertClassIsPresentOnClasspath(ProductFilterUtil.CLASS_ON_JIRA_CLASSPATH, "JIRA");
    }

    @Test
    public void testStashClassIsPresent()
    {
        Assume.assumeTrue(isAtLeastJava7());
        assertClassIsPresentOnClasspath(ProductFilterUtil.CLASS_ON_STASH_CLASSPATH, "Stash");
    }

    private void assertClassIsPresentOnClasspath(String clazz, String product)
    {
        try {
            Class.forName(clazz);
        } catch (ClassNotFoundException e) {
            fail(String.format(
                    "Class %s not found on classpath, is it no longer exported from %s? If so, %s will need to be " +
                            "updated to use an exported class from %s.",
                    clazz, product, ProductFilterUtil.class.getName(), product));
        }
    }

    private boolean isAtLeastJava7()
    {
        Pattern pattern = Pattern.compile("(\\d+)\\.(\\d+)");
        Matcher matcher = pattern.matcher(System.getProperty("java.version"));
        if (matcher.find())
        {
            int version = Integer.parseInt(matcher.group(2));
            return version >= 7;
        }
        return false;
    }

}
