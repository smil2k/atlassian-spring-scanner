package com.atlassian.plugin.spring.scanner.util;

import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import com.atlassian.plugin.spring.scanner.ProductFilter;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Utility to figure out what the currently running product is and give us it's name/filter in various ways
 */
public class ProductFilterUtil
{
    private static ProductFilterUtil INSTANCE;

    @VisibleForTesting
    static final String CLASS_ON_BAMBOO_CLASSPATH = "com.atlassian.bamboo.build.CustomPostBuildCompletedAction";
    @VisibleForTesting
    static final String CLASS_ON_CONFLUENCE_CLASSPATH = "com.atlassian.confluence.plugin.descriptor.MacroModuleDescriptor";
    @VisibleForTesting
    static final String CLASS_ON_FECRU_CLASSPATH = "com.atlassian.fisheye.spi.services.RepositoryService";
    @VisibleForTesting
    static final String CLASS_ON_JIRA_CLASSPATH = "com.atlassian.jira.bc.issue.IssueService";
    @VisibleForTesting
    static final String CLASS_ON_REFAPP_CLASSPATH = "com.atlassian.refapp.api.ConnectionProvider";
    @VisibleForTesting
    static final String CLASS_ON_STASH_CLASSPATH = "com.atlassian.stash.repository.RepositoryService";

    private final Log log = LogFactory.getLog(getClass());

    private AtomicReference<ProductFilter> filterForProduct = new AtomicReference<ProductFilter>();

    private ProductFilterUtil()
    {
    }

    /**
     * @param bundleContext - the bundle context
     * @return the ProductFilter instance that represents the currently running product.
     */
    public static ProductFilter getFilterForCurrentProduct(BundleContext bundleContext)
    {
        return getInstance().getFilterForProduct(bundleContext);
    }

    public ProductFilter getFilterForProduct(final BundleContext bundleContext)
    {
        // lazy building of known product.
        ProductFilter productFilter = filterForProduct.get();
        if (productFilter == null)
        {
            filterForProduct.compareAndSet(productFilter, detectProduct(bundleContext));
            productFilter = filterForProduct.get();
        }
        return productFilter;
    }

    /**
     * We try to detect a OSGi service that is uniquely offered by the host.
     * <p/>
     * You should extend out this method as we add support for other products
     */
    private final static Map<String, ProductFilter> PRODUCTS_TO_HOSTCLASSES = ImmutableMap.<String, ProductFilter>builder()
            .put(CLASS_ON_BAMBOO_CLASSPATH, ProductFilter.BAMBOO)
            .put(CLASS_ON_CONFLUENCE_CLASSPATH, ProductFilter.CONFLUENCE)
            .put(CLASS_ON_FECRU_CLASSPATH, ProductFilter.FECRU)
            .put(CLASS_ON_JIRA_CLASSPATH, ProductFilter.JIRA)
            .put(CLASS_ON_REFAPP_CLASSPATH, ProductFilter.REFAPP)
            .put(CLASS_ON_STASH_CLASSPATH, ProductFilter.STASH)

            .build();

    private ProductFilter detectProduct(final BundleContext bundleContext)
    {
        for (Map.Entry<String, ProductFilter> entry : PRODUCTS_TO_HOSTCLASSES.entrySet())
        {
            if (detectService(bundleContext, entry.getKey()))
            {
                log.debug("Detected product: " + entry.getValue().name());
                return entry.getValue();
            }
        }
        log.warn("Couldn't detect product, will use ProductFilter.ALL");
        return ProductFilter.ALL;
    }

    private boolean detectService(final BundleContext bundleContext, String serviceClassName)
    {
        ServiceTracker tracker = new ServiceTracker(bundleContext, serviceClassName, null);
        try
        {
            tracker.open();
            Object hostComponent = tracker.getService();
            return hostComponent != null;
        }
        finally
        {
            tracker.close();
        }

    }

    private static ProductFilterUtil getInstance()
    {
        if (null == INSTANCE)
        {
            INSTANCE = new ProductFilterUtil();
        }

        return INSTANCE;
    }

}
