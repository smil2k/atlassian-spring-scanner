package com.atlassian.plugin.spring.scanner.extension.gemini;

import com.atlassian.plugin.spring.scanner.extension.GenericOsgiServiceFactoryBean;
import com.atlassian.plugin.spring.scanner.extension.OsgiServiceFactoryBeanFactory;
import org.eclipse.gemini.blueprint.service.exporter.OsgiServiceRegistrationListener;
import org.eclipse.gemini.blueprint.service.exporter.support.DefaultInterfaceDetector;
import org.eclipse.gemini.blueprint.service.exporter.support.ExportContextClassLoaderEnum;
import org.eclipse.gemini.blueprint.service.exporter.support.OsgiServiceFactoryBean;
import org.eclipse.gemini.blueprint.service.importer.support.OsgiServiceProxyFactoryBean;
import org.osgi.framework.BundleContext;

import java.util.Map;

public class GeminiOsgiServiceFactoryBeanFactory implements OsgiServiceFactoryBeanFactory
{
    @Override
    public GenericOsgiServiceFactoryBean createExporter(final BundleContext bundleContext, final Object bean, final String beanName, final Map<String, Object> serviceProps, final Class<?>[] interfaces)
            throws Exception
    {
        serviceProps.put("org.eclipse.gemini.blueprint.bean.name", beanName);

        final OsgiServiceFactoryBean exporter = new OsgiServiceFactoryBean();
        exporter.setInterfaceDetector(DefaultInterfaceDetector.DISABLED);
        exporter.setBeanClassLoader(bean.getClass().getClassLoader());
        exporter.setBeanName(beanName);
        exporter.setBundleContext(bundleContext);
        exporter.setExportContextClassLoader(ExportContextClassLoaderEnum.UNMANAGED);
        exporter.setInterfaces(interfaces);
        exporter.setServiceProperties(serviceProps);
        exporter.setTarget(bean);
        exporter.setListeners(new OsgiServiceRegistrationListener[0]);

        exporter.afterPropertiesSet();

        return new GeminiOsgiServiceFactoryBean(exporter);
    }

    @Override
    public Class getProxyClass()
    {
        // This class is explicitly mentioned here (Rather than using Class.forName() for example in SpringDMUtil) to make sure
        // BND will generate an optional package import for this class!
        return OsgiServiceProxyFactoryBean.class;
    }
}
