package com.atlassian.plugin.spring.scanner.util;

import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;

public class BeanDefinitionChecker
{
    /**
     * copyPasta from spring-context:component-scan classes
     *
     * Check the given candidate's bean name, determining whether the corresponding
     * bean definition needs to be registered or conflicts with an existing definition.
     */
    public static boolean needToRegister(String beanName, BeanDefinition beanDefinition, BeanDefinitionRegistry registry) throws IllegalStateException
    {
        if (!registry.containsBeanDefinition(beanName))
        {
            return true;
        }

        BeanDefinition existingDef = registry.getBeanDefinition(beanName);
        BeanDefinition originatingDef = existingDef.getOriginatingBeanDefinition();
        if (originatingDef != null)
        {
            existingDef = originatingDef;
        }
        if (isCompatible(beanDefinition, existingDef))
        {
            return false;
        }
        throw new IllegalStateException("Annotation-specified bean name '" + beanName +
                "' for bean class [" + beanDefinition.getBeanClassName() + "] conflicts with existing, " +
                "non-compatible bean definition of same name and class [" + existingDef.getBeanClassName() + "]");
    }

    /**
     * copyPasta from spring-context:component-scan classes
     *
     * Determine whether the given new bean definition is compatible with
     * the given existing bean definition.
     * <p>The default implementation simply considers them as compatible
     * when the bean class name matches.
     *
     */
    public static boolean isCompatible(BeanDefinition newDefinition, BeanDefinition existingDefinition)
    {
        
        return (newDefinition.getBeanClassName().equals(existingDefinition.getBeanClassName()) ||
                newDefinition.equals(existingDefinition));
    }
}
