package com.atlassian.plugin.spring.scanner.dynamic.contexts;

import org.osgi.framework.BundleContext;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * An interface defining all common methods to be called for both Gemini Blueprint and SpringDM
 * OsgiBundleApplicationContexts.
 */
public interface OsgiBundleApplicationContext extends ConfigurableApplicationContext
{
    void setDisplayName(String symbolicName);

    void setBundleContext(BundleContext bundleContext);

    void setPublishContextAsService(boolean publish);

    void setId(String id);
}
