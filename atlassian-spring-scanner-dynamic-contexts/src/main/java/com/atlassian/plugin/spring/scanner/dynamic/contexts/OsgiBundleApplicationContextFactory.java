package com.atlassian.plugin.spring.scanner.dynamic.contexts;

import org.springframework.context.ApplicationContext;

/**
 * A singleton factory that will return the appropriate OsgiBundleApplicationContext depending on if SpringDM or Gemini
 * Blueprint is available.
 * <p/>
 * Due to the Spring Framework adopting the CADT development model (http://www.jwz.org/doc/cadt.html) the latest version
 * of Spring no longer provides SpringDM.  This became Gemini Blueprint.  Same thing, different name, same bugs (plus
 * new ones!).  This class is smart enough to figure out if SpringDM or GeminiBlueprints is in use and will return the
 * appropriate implementation hidden behind a generic interface.  Atlassian Plugins 3.1 introduces Gemini Blueprints.
 */
public class OsgiBundleApplicationContextFactory
{
    private static final String SPRING_CONTEXT_CLASS = "org.springframework.osgi.context.support.OsgiBundleXmlApplicationContext";
    private static final String GEMINI_CONTEXT_CLASS = "org.eclipse.gemini.blueprint.context.support.OsgiBundleXmlApplicationContext";
    private static OsgiBundleApplicationContextFactory INSTANCE;

    boolean useSpring = true;

    private OsgiBundleApplicationContextFactory()
    {
        try
        {
            Class.forName(SPRING_CONTEXT_CLASS);
            useSpring = true;
        }
        catch (ClassNotFoundException e)
        {
            try
            {
                Class.forName(GEMINI_CONTEXT_CLASS);
                useSpring = false;
            }
            catch (ClassNotFoundException cnfe)
            {
                throw new RuntimeException("Unabled to load springDM nor Gemini Blueprint classes", cnfe);
            }
        }
    }

    public OsgiBundleApplicationContext getOsgiBundleApplicationContext(String[] springConfigPaths, ApplicationContext parentContext)
    {
        if (useSpring)
        {
            return new SpringOsgiBundleXmlApplicationContext(springConfigPaths, parentContext);
        }
        else
        {
            return new GeminiOsgiBundleXmlApplicationContext(springConfigPaths, parentContext);
        }
    }

    public static OsgiBundleApplicationContextFactory getInstance()
    {
        if (INSTANCE == null)
        {
            INSTANCE = new OsgiBundleApplicationContextFactory();
        }
        return INSTANCE;
    }
}
