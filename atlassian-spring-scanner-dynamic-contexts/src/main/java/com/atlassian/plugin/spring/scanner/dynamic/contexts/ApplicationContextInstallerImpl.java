package com.atlassian.plugin.spring.scanner.dynamic.contexts;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.event.events.PluginContainerRefreshedEvent;
import com.atlassian.plugin.osgi.spring.DefaultSpringContainerAccessor;
import org.osgi.framework.BundleContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Date;

/**
 * This class can install new Spring application contexts into a running Atlassian plugin
 */
public class ApplicationContextInstallerImpl implements DynamicContext.Installer
{
    private final EventPublisher eventPublisher;
    private final BundleContext bundleContext;
    private final String pluginKey;

    ApplicationContextInstallerImpl(final EventPublisher eventPublisher, final BundleContext bundleContext, final String pluginKey)
    {
        this.eventPublisher = eventPublisher;
        this.bundleContext = bundleContext;
        this.pluginKey = pluginKey;
    }


    /**
     * This method will create and install a new child application context into the plugin.
     * <p/>
     * As a side effect a {@link com.atlassian.plugin.event.events.PluginRefreshedEvent} will be generated for this new
     * child context.
     * <p/>
     * By calling this method you are able to have the plugin auto-wire components such as actions and web-items from
     * the child context instead of the originating parent context.  This will allow you to dynamically grow and shrink
     * the components that your plugin gives off to the world based on external events such as licencing or memory
     * passivisation.
     *
     * @param springConfigPaths the paths to find spring configuration files on the class path
     * @param parentContext the parent context, typically the context that came with your plugin
     * @return the newly created child context
     */
    @Override
    public ConfigurableApplicationContext useContext(String[] springConfigPaths, ApplicationContext parentContext)
    {
        Date now = new Date();
        final OsgiBundleApplicationContext newContext = OsgiBundleApplicationContextFactory.getInstance().getOsgiBundleApplicationContext(springConfigPaths, parentContext);
        newContext.setDisplayName(bundleContext.getBundle().getSymbolicName());
        newContext.setId(bundleContext.getBundle().getBundleId() + "." + now.toString());
        newContext.setBundleContext(bundleContext);
        newContext.setPublishContextAsService(false);
        newContext.refresh();
        newContext.start();

        publishNewContext(newContext);

        return newContext;
    }

    /**
     * This method can "close" a child application context (typically one created with {@link #useContext(String[],
     * org.springframework.context.ApplicationContext)} and set in the replacement application context instead.
     * Typically the replacement would be the parent of the child context to be closed but it doesnt always have to be.
     *
     * @param childContext the child context to close.
     * @param replacementContext the context to replace the child context with.  Typically this can be its parent
     */
    @Override
    public void closeAndUseContext(ConfigurableApplicationContext childContext, ApplicationContext replacementContext)
    {
        childContext.close();
        if (replacementContext != null)
        {
            publishNewContext(replacementContext);
        }
    }

    private void publishNewContext(final ApplicationContext newContext)
    {
        //
        // cause the OsgiPlugin to get our new context.  This is really important.  This is how it knows how
        // to wire future code etc..
        DefaultSpringContainerAccessor containerAccessor = new DefaultSpringContainerAccessor(newContext);
        PluginContainerRefreshedEvent event = new PluginContainerRefreshedEvent(containerAccessor, pluginKey);

        eventPublisher.publish(event);
    }

}
